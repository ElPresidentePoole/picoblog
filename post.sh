#!/bin/bash

set_variables() {
	[ -z $EDITOR ] && EDITOR=nvim
	[ -z $PB_SRC_DIR ] && PB_SRC_DIR=$HOME/.pb/src
}

new_post() {
	ARTICLE_COUNT=$(ls -1 $PB_SRC_DIR/articles/ | wc -l)
	DATE=$(date +"%a, %d %b %Y %T %Z")
	TMPFILE=/tmp/post${RANDOM}.md
	printf "# Title Here



<!-- date: $DATE -->" > $TMPFILE
	$EDITOR $TMPFILE
	cp $TMPFILE "${PB_SRC_DIR}/articles/${ARTICLE_COUNT}.md" && echo "${PB_SRC_DIR}/articles/${ARTICLE_COUNT}.md created!"
}

set_variables
new_post
