#!/bin/bash
requirements_check() {
	if ! command -v pandoc > /dev/null 2>&1 ; then
		echo "Pandoc is required for pb."
		exit 1
	elif [ -z $EDITOR ] && ! command -v vim > /dev/null 2>&1 ; then
		echo "Either the EDITOR envvar needs to be defined with a valid editor or vim needs to be installed."
		exit 1
	fi
}

set_variables() {
	[ -z $PB_SRC_DIR ] && PB_SRC_DIR=$HOME/.pb/src
	[ -z $PB_BIN_DIR ] && PB_BIN_DIR=$HOME/.pb/bin
	[ -z $PB_SITE_NAME] && PB_SITE_NAME="picoblog demo"
	[ -z $PB_RSS_TITLE ] && PB_RSS_TITLE="example rss title"
	[ -z $PB_RSS_LINK ] && PB_RSS_LINK="google.com"
	[ -z $PB_RSS_LANG ] && PB_RSS_LANG="en-us"
	[ -z $PB_RSS_DESC ] && PB_RSS_DESC="example rss description"
}

build_pages() {
	# add blog to applicable page
	if [ ! -z $PB_BLOG_PAGE ] ; then
		insert_articles
	fi
	# build documents in /pages
	[ ! -d $PB_SRC_DIR/pages ] && mkdir -p $PB_SRC_DIR/pages
	[ ! -d $PB_BIN_DIR/ ] && mkdir -p $PB_BIN_DIR/
	for page in $(find $PB_SRC_DIR/pages/ -type f -name '*.md'); do
		FILE=${page##*/}
		TITLE="${FILE%%.*}"
		DEST="${TITLE}.html"
		printf "Building $DEST..."
		# if this is our blog page then we're going to build our modified .md instead of the one straight from src
		echo "pb_blog_page $PB_BLOG_PAGE file $FILE" # FIXME: why doesn't our blog page contain the latest posts?
		if [ "$PB_BLOG_PAGE" = "$FILE" ] ; then
			echo "and they're EQUAL!"
			pandoc --self-contained --resource-path=$PB_SRC_DIR/pages --css $PB_SRC_DIR/style.css -f markdown+header_attributes -o $PB_BIN_DIR/$DEST --title-prefix="$PB_SITE_NAME" /tmp/$PB_BLOG_PAGE
		else
			pandoc --self-contained --resource-path=$PB_SRC_DIR/pages --css $PB_SRC_DIR/style.css -f markdown+header_attributes -o $PB_BIN_DIR/$DEST --title-prefix="$PB_SITE_NAME" "$page"
		fi
		printf "Done!\n"
	done
	# build documents in /articles
	# TODO: are *articles* being built?
	[ ! -d $PB_SRC_DIR/articles ] && mkdir -p $PB_SRC_DIR/articles
	[ ! -d $PB_BIN_DIR/articles ] && mkdir -p $PB_BIN_DIR/articles
	for page in $(find $PB_SRC_DIR/articles/ -type f -name '*.md'); do
		FILE=${page##*/}
		TITLE="${FILE%%.*}"
		DEST="${TITLE}.html"
		printf "Building $DEST..."
		pandoc --self-contained --resource-path=$PB_SRC_DIR/pages --css $PB_SRC_DIR/style.css -f markdown+header_attributes -o $PB_BIN_DIR/articles/$DEST --title-prefix="$PB_SITE_NAME" $page
		printf "Done!\n"
	done
}

echo_articles_md() {
	for article in $(ls -1 $PB_SRC_DIR/articles/*.md | tac); do
		P_TITLE="$(head -1 $article | sed 's/# //g')"
		FILE=${article##*/}
		TITLE="${FILE%%.*}"
		P_LOC="./articles/${TITLE}.html"
		printf "\n\n[$P_TITLE]($P_LOC)"
	done
}

echo_rss() {
	# variables we're going to use later
	RSS_HEADER="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\">
<channel>
<generator>pb</generator>"
	RSS_FOOTER="</channel>
</rss>"
	printf "$RSS_HEADER"
	printf "\t<title>$PB_RSS_TITLE</title>\n"
	printf "\t<link>$PB_RSS_LINK</link>\n"
	printf "\t<language>$PB_RSS_LANG</language>\n"
	printf "\t<description>$PB_RSS_DESC</description>\n"
	for article in $(ls -1 $PB_SRC_DIR/articles/*.md); do
		P_TITLE="$(head -1 $article | sed 's/# //g')"
		P_DESC="$(tail -n+3 $article | head -n-2)"
		P_DATE="$(tail -n1 $article | sed 's/<!-- date: //g' | sed 's/ -->//g')"
		printf "\t<item>\n"
		printf "\t\t<title>$P_TITLE</title>\n"
		printf "\t\t<description>$P_DESC</description>\n"
		printf "\t\t<pubDate>$P_DATE</pubDate>\n"
		printf "\t</item>\n"
	done
	echo "$RSS_FOOTER"
}

build_rss() {
	echo_rss > $PB_BIN_DIR/rss.xml
}


print_help() {
	echo "build.sh [command]"
	echo "commands:"
	#echo "	-v picoblog version"
	echo "	-h build.sh help (this dialog)"
	#echo "	-c clean (delete $PB_BIN_DIR and subfolders)"
}

insert_articles() {
	cp $PB_SRC_DIR/pages/$PB_BLOG_PAGE /tmp/${PB_BLOG_PAGE}
	echo "$(echo_articles_md)" >> /tmp/${PB_BLOG_PAGE}
}
 
requirements_check
set_variables
build_pages
build_rss
