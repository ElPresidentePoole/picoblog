pb - picoblog
=============
pico- is a prefix denoting a factor of 10^-12

picoblog (referred to by the binary's name, "pb") is a minimalist tool for
creating statically generated websites using Markdown and pandoc.  True to its
name, it supports blog-like feeds, but it is optional.

This script uses Bash and uses Bash-isms.  Because of this, it is not strictly
POSIX compliant.

Features
--------
- RSS 2.0 support
- CSS support
- Runs 100% in bash

TODO
----
- Tutorial and documentation
